ElFinderIntegration
===================

Integrates [elFinder](https://github.com/Studio-42/elFinder/) file manager into [Yii framework](http://www.yiiframework.com/)

Extension uses [elFinder nightly builds](https://github.com/nao-pon/elFinder-nightly/) as elFinder sources.

Extension is under development, so, sometimes may be broken or buggy. Be attentive and watch for updates if something works wrong.
