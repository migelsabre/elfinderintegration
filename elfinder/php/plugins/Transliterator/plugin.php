<?php
/**
 * elFinder Plugin Transliterator.
 * Transliterates cyrillic file names to latin equivalents and replaces other characters with $replacement string
 *
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2014, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 * @package elfinder
 *
 * ex. binding, configure on connector options
 *	$opts = array(
 *		'bind' => array(
 *			'mkdir.pre mkfile.pre rename.pre' => array(
 *				'Plugin.Transliterator.cmdPreprocess'
 *			),
 *			'upload.presave' => array(
 *				'Plugin.Transliterator.onUpLoadPreSave'
 *			)
 *		),
 *		// global configure (optional)
 *		'plugin' => array(
 *			'Transliterator' => array(
 *              'enable'   => true,
 *              'source' => 'ё,й,ц,у,к,е,н,г,ш,щ,з,х,ъ,ф,ы,в,а,п,р,о,л,д,ж,э,я,ч,с,м,и,т,ь,б,ю, ,[,],Ё,Й,Ц,У,К,Е,Н,Г,Ш,Щ,З,Х,Ъ,Ф,Ы,В,А,П,Р,О,Л,Д,Ж,Э,Я,Ч,С,М,И,Т,Ь,Б,Ю',
 *              'target'  => 'yo,j,ts,u,k,e,n,g,sh,shch,z,h,,f,y,v,a,p,r,o,l,d,zh,e,ya,ch,s,m,i,t,,b,yu,_,_,_,yo,j,ts,u,k,e,n,g,sh,shch,z,h,,f,y,v,a,p,r,o,l,d,zh,e,ya,ch,s,m,i,t,,b,yu',
 *              'replacement'  => '_'
 *			)
 *		),
 *		// each volume configure (optional)
 *		'roots' => array(
 *			array(
 *				'driver' => 'LocalFileSystem',
 *				'path'   => '/path/to/files/',
 *				'URL'    => 'http://localhost/to/files/'
 *				'plugin' => array(
 *					'Transliterator' => array(
 *						'enable'   => true,
 *                      'source' => 'ё,й,ц,у,к,е,н,г,ш,щ,з,х,ъ,ф,ы,в,а,п,р,о,л,д,ж,э,я,ч,с,м,и,т,ь,б,ю, ,[,],Ё,Й,Ц,У,К,Е,Н,Г,Ш,Щ,З,Х,Ъ,Ф,Ы,В,А,П,Р,О,Л,Д,Ж,Э,Я,Ч,С,М,И,Т,Ь,Б,Ю',
 *                      'target'  => 'yo,j,ts,u,k,e,n,g,sh,shch,z,h,,f,y,v,a,p,r,o,l,d,zh,e,ya,ch,s,m,i,t,,b,yu,_,_,_,yo,j,ts,u,k,e,n,g,sh,shch,z,h,,f,y,v,a,p,r,o,l,d,zh,e,ya,ch,s,m,i,t,,b,yu',
 *                      'replacement'  => '_'
 *					)
 *				)
 *			)
 *		)
 *	);
 *
 */
class elFinderPluginTransliterator {
	const PLUGIN_NAME = 'Transliterator';
	/**
	 * @var array plugin options
	 */
	private $opts = array();

	/**
	 * @param array $opts
	 */
	public function __construct($opts) {
		$defaults = array(
			/* For control by volume driver */
			'enable'   => true,
			/* Source table */
			'source' => 'ё,й,ц,у,к,е,н,г,ш,щ,з,х,ъ,ф,ы,в,а,п,р,о,л,д,ж,э,я,ч,с,м,и,т,ь,б,ю, ,[,],Ё,Й,Ц,У,К,Е,Н,Г,Ш,Щ,З,Х,Ъ,Ф,Ы,В,А,П,Р,О,Л,Д,Ж,Э,Я,Ч,С,М,И,Т,Ь,Б,Ю',
			/* Table of replacements */
			'target'  => 'yo,j,ts,u,k,e,n,g,sh,shch,z,h,,f,y,v,a,p,r,o,l,d,zh,e,ya,ch,s,m,i,t,,b,yu,_,_,_,yo,j,ts,u,k,e,n,g,sh,shch,z,h,,f,y,v,a,p,r,o,l,d,zh,e,ya,ch,s,m,i,t,,b,yu',
			/* Char to replace unknown chars */
			'replacement'  => '_'
		);
		$this->opts = array_merge($defaults, $opts);
	}

	/**
	 * @param string $cmd
	 * @param array $args
	 * @param elFinder $elfinder
	 * @param elFinderVolumeDriver $volume
	 * @return bool
	 */
	public function cmdPreprocess($cmd, &$args, $elfinder, $volume) {
		$opts = $this->getOpts($volume);
		if (! $opts['enable']) {
			return false;
		}
		if (isset($args['name'])) {
			$args['name'] = $this->transliterate($args['name'], $opts);
		}
		return true;
	}

	/**
	 * @param string $path
	 * @param string $name
	 * @param $src
	 * @param elFinder $elfinder
	 * @param elFinderVolumeDriver $volume
	 * @return bool
	 */
	public function onUpLoadPreSave(&$path, &$name, $src, $elfinder, $volume) {
		$opts = $this->getOpts($volume);
		if (! $opts['enable']) {
			return false;
		}
		$name = $this->transliterate($name, $opts);
		return true;
	}

	/**
	 * @param elFinderVolumeDriver $volume
	 * @return array
	 */
	private function getOpts($volume) {
		$opts = $this->opts;
		if (is_object($volume)) {
			$volOpts = $volume->getOptionsPlugin(self::PLUGIN_NAME);
			if (is_array($volOpts)) {
				$opts = array_merge($this->opts, $volOpts);
			}
		}
		return $opts;
	}

	/**
	 * @param string $str
	 * @param array $opts
	 * @return string
	 */
	private function transliterate($str, $opts) {
		$source = explode(',', $opts['source']);
		$target = explode(',', $opts['target']);
		return trim(preg_replace("/([^A-z0-9])+/ui", "$1", preg_replace( "/[^a-zA-Z0-9\\.".preg_quote($opts['replacement'])."]+/iu", $opts['replacement'], str_replace($source, $target, trim($str)))), '_-.'.$opts['replacement']);
	}
}
