<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2014, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */

if(!Yii::getPathOfAlias('ElFinderIntegration')) {
	Yii::setPathOfAlias('ElFinderIntegration', dirname(__FILE__));
}

function ElFinderIntegrationAutoLoad($class) {
	if(0 === strpos($class, 'ElFinderIntegration')) {
		require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . $class . '.php');
	} else if(0 === strpos($class, 'elFinder')) {
		require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'elfinder' . DIRECTORY_SEPARATOR . 'php' . DIRECTORY_SEPARATOR . $class . '.class.php');
	}
}
Yii::registerAutoloader('ElFinderIntegrationAutoLoad');
