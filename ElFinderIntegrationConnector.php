<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2012, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */

class ElFinderIntegrationConnector extends CAction {
	public $options = array();
	/**
	 * @return array
	 */
	public function defaults() {
		return array(
			'roots' => array(
				0 => array(
					'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
					'path'          => '../files/',         // path to files (REQUIRED)
					'URL'           => dirname($_SERVER['PHP_SELF']) . '/../files/', // URL to files (REQUIRED)
					'accessControl' => array($this, 'access'),             // disable and hide dot starting files (OPTIONAL)
				)
			),
		);
	}
	/**
	 * Собственно код виджета
	 */
	public function run() {
		$this->getController()->layout = false;
		$this->raiseEvent('onBeforeInit', new CEvent($this, array('options' => &$this->options)));
		$options = $this->options;
		defined('ELFINDER_IMG_PARENT_URL') or define('ELFINDER_IMG_PARENT_URL', Yii::app()->getAssetManager()->getPublishedUrl(dirname(__FILE__) . '/elfinder/resources'));
		$connector = new elFinderConnector(new elFinder($options));
		$src    = $_SERVER["REQUEST_METHOD"] == 'POST' ? $_POST : $_GET;
		$cmd    = isset($src['cmd']) ? $src['cmd'] : '';
		$this->raiseEvent('onBeforeCommand', new CEvent($this, array('command' => $cmd, 'connector' => $connector)));
		$connector->run();
		$this->raiseEvent('onAfterCommand', new CEvent($this, array('command' => $cmd, 'connector' => $connector)));
	}

	/**
	 * @param \CEvent $event
	 */
	public function onBeforeInit($event) {}

	/**
	 * @param \CEvent $event
	 */
	public function onBeforeCommand($event) {}

	/**
	 * @param \CEvent $event
	 */
	public function onAfterCommand($event) {}

	/**
	 * @param string $attr
	 * @param string $path
	 * @param $data
	 * @param $volume
	 * @return boolean
	 */
	public function access($attr, $path, $data, $volume) {
		return strpos(basename($path), '.') === 0   // if file/folder begins with '.' (dot)
				? !($attr == 'read' || $attr == 'write')  // set read+write to false, other (locked+hidden) set to true
				: ($attr == 'read' || $attr == 'write');  // else set read+write to true, locked+hidden to false
	}
}
