<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2012, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */
/**
 * Шаблон страницы файл-менеджера ElFinder
 * @var array $options
 */

?>
<p>Будьте внимательны - все имена загружаемых файлов и созданных папок подвергаются транслитерации!</p>
<p>Старайтесь раскладывать файлы по папкам, подбирайте понятные имена папок.</p>
<?php

Yii::app()->getController()->widget('ElFinderIntegration.ElFinderIntegration', array(
	'options' => $options
));
