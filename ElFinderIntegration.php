<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2012, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */

class ElFinderIntegration extends CWidget {
	private $_resources;
	public $options = array();
	public $url;
	public $selector;

	public function init() {
		/**
		 * @var CClientScript $cs
		 */
		$cs = Yii::app()->clientScript;
		/**
		 * @var CAssetManager $am
		 */
		$am = Yii::app()->assetManager;
		$this->_resources = $am->publish(dirname(__FILE__) . '/elfinder/resources', false, -1, defined('YII_DEBUG') && YII_DEBUG);
		$cs->registerCoreScript('jquery.ui');
		$options = array_replace_recursive($this->defaults(), $this->options);
		foreach(array('elfinder.min.css', 'theme.css') as $css) {
			$cs->registerCssFile($this->_resources . '/css/' . $css);
		}
		foreach(array('elfinder.min.js', 'i18n/elfinder.' . ($options['lang'] ? $options['lang'] : 'ru') . '.js') as $js) {
			$cs->registerScriptFile($this->_resources . '/js/' . $js);
		}
	}

	public function run() {
		if(empty($this->selector)) {
			$this->selector = $this->getId();
			echo CHtml::tag('div', array('id' => $this->getId()), '', true);
		}
		$options = array_replace_recursive($this->defaults(), $this->options);
		if(!empty($this->url)) {
			$options['url'] = $this->url;
		}
		$options = CJavaScript::encode($options);
		$script = <<<ElfinderIntegration
	$('#{$this->selector}').elfinder({$options}).elfinder('instance');
ElfinderIntegration;
		Yii::app()->clientScript->registerScript(__CLASS__ . $this->getId(), $script);
	}

	/**
	 * @return array
	 * @example пример для интеграции с tinyMCE
	 *  getFileCallback: function(url) {
	 *        tinyMCEPopup.getWindowArg("pWindow").document.forms[0].elements[tinyMCEPopup.getWindowArg("urlInput")].value = url;
	 *        tinyMCEPopup.close();
	 *    }
	 * а это для file_browser_callback : elfinderCallback,
	 *            quick_upload_callback : elfinderCallback,
	 * var elfinderCallback = function(field_name, url, type, win) {
	 *        tinyMCE.activeEditor.windowManager.open({
	 *            file : '/backend/vendors/elfinder?nolayout=1&tinymce=1',
	 *            title : 'Файловый менеджер elFinder 2.0',
	 *            width : 1000,
	 *            height : 700,
	 *            resizable : "yes",
	 *            scrollbars : "yes",
	 *            inline : "yes",  // This parameter only has an effect if you use the inlinepopups plugin!
	 *            close_previous : "no"
	 *        }, {
	 *            pWindow : win,
	 *            urlInput : field_name
	 *        });
	 *    };
	 */
	public function defaults() {
		return array(
			'lang' => 'ru',
			'url' => 'php/connector.php'
		);
	}
}
