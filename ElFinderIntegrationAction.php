<?php
/**
 * @author Mihail Sablin <mihail@webprogrammist.com>
 * @copyright Copyright (c) 2013, Mihail Sablin
 * @licence http://www.opensource.org/licenses/mit-license.php MIT licence
 */

class ElFinderIntegrationAction extends CAction {
	public $title = '';
	public $connectorUrl = '';
	public $tinymceVar = 'tinymce';
	public $tinymcePath = 'ext.tinymce.resources';
	public $view = 'ElFinderIntegration.views.elfinder';
	public $options = array();

	public function run() {
		$this->raiseEvent('onBeforeRender', new CEvent($this));
		$tinymce = empty($_GET[$this->tinymceVar]) ? false : true;
		$this->controller->pageTitle = $this->title;
		$cs = \Yii::app()->clientScript;
		$cs->registerCoreScript('jquery.ui');
		$am = \Yii::app()->assetManager;
		$options = array_replace_recursive(
			array(
				'url' => $this->connectorUrl,
				'lang' => \Yii::app()->language,
				'height' => 600,
				'customData' => \Yii::app()->request->enableCsrfValidation ? array(
					\Yii::app()->getRequest()->csrfTokenName => \Yii::app()->getRequest()->getCsrfToken(),
				) : array(),
			),
			$this->options
		);
		if($tinymce) {
			$mceAssets = $am->publish(\Yii::getPathOfAlias($this->tinymcePath), false, -1, defined('YII_DEBUG') && YII_DEBUG);
			$cs->registerScriptFile($mceAssets . '/tiny_mce_popup.js');
			$options['getFileCallback'] = new CJavaScriptExpression('function(url) {
				var input = tinyMCEPopup.getWindowArg("pWindow").document.forms[0].elements[tinyMCEPopup.getWindowArg("urlInput")];
				if("undefined" !== typeof(url.url)) {
					url = url.url;
				}
				input.value = url;
				tinyMCEPopup.close();
			}');
		}
		$this->controller->render($this->view, array('options' => $options));
	}

	/**
	 * @param CEvent $event
	 */
	public function onBeforeRender($event) {}
}
